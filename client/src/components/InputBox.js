import React from 'react';
import { connect } from 'react-redux';
import { addNewTodo } from '../actions';


class InputBox extends React.Component {
    state = {newTodo: ''}

    onSubmit = (e) => {
        e.preventDefault();

        this.props.addNewTodo(this.state.newTodo);
        this.setState({newTodo: ''});
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <div className="field">
                    <label className="label">New TODO</label>
                    <div className="columns">
                        <div className="column">
                            <div className="control">
                                <input 
                                    type="text" 
                                    placeholder="Add a new todo item..."
                                    className="input"
                                    value={this.state.newTodo} 
                                    onChange={(e) => {this.setState({newTodo: e.target.value});}} 
                                />
                            </div>
                        </div>
                        <div className="column is-1">
                            <button className="button is-primary">Add</button>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}

export default connect(null, { addNewTodo })(InputBox);