import React from 'react';
import { connect } from 'react-redux';
import { 
    swapCompletedState, 
    fetchTodoList,
    deleteCompletedTodos 
} from '../actions';

class TodoList extends React.Component {
    componentDidMount() {
        this.props.fetchTodoList();
    }

    changeCompleteState = (todo) => {
        this.props.swapCompletedState(todo);
    };

    isDeleteButtonActive() {
        return this.props.todos.reduce((acc, cur) => {
            return acc || cur.isCompleted;
        }, false);
    }

    renderTodoList() {
        return this.props.todos.map((todo, index) => {
            const textClasses = 'column ' + (todo.isCompleted ? 'todo-completed' : '');

            return (
                <div 
                    className="list-item" 
                    key={todo.id} 
                    onClick={(e) => this.changeCompleteState(index)}>
                    <div className="columns">
                        <div className="column is-1">
                            <div className="field">
                                <div className="control">
                                    <input 
                                        type="checkbox"
                                        className="state-checkbox"
                                        onChange={() => {this.changeCompleteState(index)}}
                                        checked={todo.isCompleted}/>
                                </div>
                            </div>
                        </div>
                        <div className={textClasses}>
                            {todo.text}
                        </div>
                    </div>
                </div>
            )
        });
    }

    render() {
        const isDisabled = !this.isDeleteButtonActive();
        return (
            <div>
                <div className="list todo-list-container">
                    {this.renderTodoList()}
                </div>
                <p>
                    <button 
                        onClick={this.props.deleteCompletedTodos}
                        disabled={isDisabled}
                        className="button is-danger">Delete completed</button>
                </p>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        todos: state.todos
    }
}

export default connect(
    mapStateToProps, 
    { 
        swapCompletedState, 
        fetchTodoList,
        deleteCompletedTodos
    }
)(TodoList);