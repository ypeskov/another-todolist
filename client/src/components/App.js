import React from 'react';


import InputBox from './InputBox';
import TodoList from './TodoList';

class App extends React.Component {
    render() {
        return (
            <section className="section">
                <div className="container">
                    <InputBox />
                    <TodoList />
                </div>
            </section>
        );
    }
}

export default App;