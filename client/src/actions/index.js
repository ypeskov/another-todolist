import _ from 'lodash';
import { 
    ADD_NEW_TODO,
    SWAP_COMPLETED_STATE,
    FETCH_TODO_LIST
} from './types';
import todos from '../apis/todos';


export const addNewTodo = newTodo => async dispatch => {
    const response = await todos.post('/todos', {
        isCompleted: false,
        text: newTodo
    });

    dispatch({
        type: ADD_NEW_TODO,
        payload: response.data
    });
};

export const swapCompletedState = index => async (dispatch, getState) => {
    let todo = getState().todos[index];
    todo.isCompleted = !todo.isCompleted;
    await todos.patch(`/todos/${todo.id}`, todo);

    dispatch({
        type: SWAP_COMPLETED_STATE,
        payload: {index, todo}
    });
};

export const fetchTodoList = () => async dispatch => {
    const response = await todos.get('/todos');

    dispatch({
        type: FETCH_TODO_LIST,
        payload: response.data
    });
}

export const deleteCompletedTodos = () => async (dispatch, getState) => {
    const ids = _.compact(getState().todos.map(todo => {
        return todo.isCompleted === true ? `${todo.id}` : null;
    })).join(',');

    await todos.delete(`/todos/${ids}`);

    dispatch(fetchTodoList());
};