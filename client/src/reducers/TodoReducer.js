import { 
    ADD_NEW_TODO,
    SWAP_COMPLETED_STATE,
    FETCH_TODO_LIST
} from '../actions/types';

const defaultState = [];

export default(state=defaultState, action) => {
    switch(action.type) {
        case ADD_NEW_TODO:
            return [...state, action.payload];

        case SWAP_COMPLETED_STATE:
            let newState = [...state];
            newState[action.payload.index] = action.payload.todo;

            return newState;

        case FETCH_TODO_LIST:
            return [...action.payload];

        default:
            return state;
    }
};